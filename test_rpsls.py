###======================================================================#
#                                                                        #
# Copyright (C) 2015 Carlos Augusto de Souza Braga                       #
# <CASBraga@Gmail.com>                                                   #
#                                                                        #
#  This program is free software: you can redistribute it and/or modify  #
#  it under the terms of the GNU General Public License as published by  #
#  the Free Software Foundation, either version 3 of the License, or     #
#  (at your option) any later version.                                   #
#                                                                        #
#  This program is distributed in the hope that it will be useful,       #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#  GNU General Public License for more details.                          #
#========================================================================#

import rpsls

# creates Rpsls object
my_game = rpsls.Rpsls(3)

# gets user choice
choice = raw_input("")
my_game.game_logic(choice)

choice = raw_input("")
my_game.game_logic(choice)

choice = raw_input("")
my_game.game_logic(choice)

choice = raw_input("")
my_game.game_logic(choice)

choice = raw_input("")
my_game.game_logic(choice)

choice = raw_input("")
my_game.game_logic(choice)

choice = raw_input("")
my_game.game_logic(choice)

choice = raw_input("")
my_game.game_logic(choice)

# starts new game with diferent number of rounds
my_game.best3()

my_game.best5()

my_game.best7()
