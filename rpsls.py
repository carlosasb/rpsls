###======================================================================#
#                                                                        #
# Copyright (C) 2015 Carlos Augusto de Souza Braga                       #
# <CASBraga@Gmail.com>                                                   #
#                                                                        #
#  This program is free software: you can redistribute it and/or modify  #
#  it under the terms of the GNU General Public License as published by  #
#  the Free Software Foundation, either version 3 of the License, or     #
#  (at your option) any later version.                                   #
#                                                                        #
#  This program is distributed in the hope that it will be useful,       #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#  GNU General Public License for more details.                          #
#========================================================================#

import random

class Rpsls:
    """
    Class Rpsls is a game of Rock, paper, scissors, lizard and Spock.
    Choices are made using the following chart:

    0 - rock
    1 - Spock
    2 - paper
    3 - lizard
    4 - scissors    
    
    Contains:
    
    Global variables:
        best_of    => integer containing the number of rounds to be played 
                      for each game 
        c_score    => integer containing the cpu score during a game
        p_score    => integer containing the player score during a game
        game_count => integer counting the total number of games for a session
        r_count    => integer counting the number of rounds played in a given match
        err        => boolean True for valid inpu or False for invalid input;
                      initially set to True
    Methods:
        private methods (aka helper functions):
            __init__
            name_to_number(name)
            number_to_name(number)
            new_game(n)
        public methods (aka event handlers):
            best_of_three()
            best_of_five()
            best_of_seven()
            game_logic(palyer_choice)
    """
    
    # global variables
    best_of    = None
    c_score    = None
    p_score    = None
    game_count = None
    r_count    = None
    err        = None

    # class constructor
    def __init__(self, n_rounds):
        """
        Contructor function to initialize global variables
        
        n_rounds => integer containing initial number of rounds 
                    to play for a single match
        """
        
        # initialize global variables
        self.game_count = 0
        self.err        = True
        self.new_game(n_rounds)
        
    # helper functions
    def new_game(self, n):
        """
        Starts or restarts a game of rpsls
        
        n => integer containing initial number of rounds 
             to play for a single match
        """
        
        # initializes global variables for any given game
        self.best_of = n
        self.c_score = 0
        self.p_score = 0
        self.r_count = 0
                
        # print new game message
        if (self.game_count > 0):
            print "New game is a best of", self.best_of
            print "Choose wisely..."
            print ""
        else:
            print "Welcome to Rock, paper scissors, lizard, Spock!"
            print "Your choices are (naturally):"
            print ""
            print "rock"
            print "Spock"
            print "paper"
            print "lizard"
            print "scissors"
            print "MENSAGEM INTEIRA DE RPSLS AQUI!!!! SUBSTITUIR"
            print "It's a best of", self.best_of
            print ""
            
    def name_to_number(self, name):
        """
        Converts the string <name> to an integer number,
        according to the following chart:
            
        0 - rock
        1 - Spock
        2 - paper
        3 - lizard
        4 - scissors
        """
        
        # Use of the python if/elif/else statement to handle all names,
        # set err = True if valid input
        if (name == "rock"):
            self.err = True
            return 0
        elif (name == "Spock"):
            self.err = True
            return 1
        elif (name == "paper"):
            self.err = True
            return 2
        elif (name == "lizard"):
            self.err = True
            return 3
        elif (name == "scissors"):
            self.err = True
            return 4
        else:
            # if not a valid string print error message and
            # set err = False
            print "Invalid choice! Choose from:"
            print ""
            print "rock"
            print "Spock"
            print "paper"
            print "lizard"
            print "scissors"
            self.err = False
            return -999

    def number_to_name(self, number):
        """
        Converts the integer <number> to a string,
        according to the following chart:
            
        0 - rock
        1 - Spock
        2 - paper
        3 - lizard
        4 - scissors
        """
        
        # Use of the python if/elif/else statement to handle all names
        # No need to handle errors since input is controled by the program
        # itself
        if (number == 0):
            return "rock"
        elif (number == 1):
            return "Spock"
        elif (number == 2):
            return "paper"
        elif (number == 3):
            return "lizard"
        else: 
            return "scissors"

    # event handlers
    def best3(self):
        """
        Set the new game as a best of 3
        """
        
        # add one to game_count
        self.game_count += 1
        
        # start new game
        self.new_game(3)
    
    def best5(self):
        """
        """
        
        # add one to game_count
        self.game_count += 1
        
        # start new game
        self.new_game(5)
        
    def best7(self):
        """
        """
        
        # add one to game_count
        self.game_count += 1
        
        # start new game
        self.new_game(7)
    
    def game_logic(self, player_choice): 
        """
        Computes the winner of the game given the player's choice
        
        player_choice => string containing the player's choice
        """

        # call to the function name-to_number generates the number num
        # corresponding to player_choice
        pnum = self.name_to_number(player_choice)
        
        # handles invalid input
        if (self.err):
            # game continues, add one to r_count
            self.r_count += 1
            
            print "Player chooses", player_choice
            
            # generates a random integer in the range 0 <= n < 5
            # and prints message indicating computer choice, given by cnum
            cnum = random.randrange(0,5)
            print "Computer chooses", self.number_to_name(cnum)
            
            # compute difference of comp_number and player_number modulo five
            # use if/elif/else to determine winner, print winner message
            diff = (pnum - cnum) % 5
            if (diff == 0):
                print "Player and computer tie!"
            elif (diff <=2 and diff >= 1):
                print "Player wins!"
                self.p_score += 1
            else:
                print "Player loses!"
                self.c_score += 1

            # prints scores
            print "Player:", self.p_score
            print "Computer:", self.c_score
                
            # victory condition for an entire match is calculated taking the next even number, 
            # counting from best_of, and dividing by 2
            v_condition = (self.best_of + 1) / 2

            # if p_score == v_condition, player wins and a new game starts
            # if c_score == v_condition, cpu wins and a new game starts
            # if r_count == best_of and c_score == p_score, game tied and new game starts
            if (self.p_score == v_condition):
                self.game_count += 1
                print "Player wins best of", self.best_of, ",", self.p_score, "to", self.c_score
                print ""
                self.new_game(3)
            elif (self.c_score == v_condition):
                self.game_count += 1
                print "Player loses best of", self.best_of, ",", self.c_score, "to", self.p_score
                print ""
                self.new_game(3)
            elif (self.r_count == self.best_of and self.c_score < self.p_score):
                self.game_count += 1
                print "Player wins best of", self.best_of, ",", self.p_score, "to", self.c_score
                print ""
                self.new_game(3)
            elif (self.r_count == self.best_of and self.c_score > self.p_score):
                self.game_count += 1
                print "Player loses best of", self.best_of, ",", self.c_score, "to", self.p_score
                print ""
                self.new_game(3)
            elif (self.r_count == self.best_of and self.c_score == self.p_score):
                print "Best of", self.best_of, "is now a best of,", self.best_of + 2
                self.best_of += 2
                print "Choose again!"
                print ""
            else:
                print "Choose again!"
                print ""
